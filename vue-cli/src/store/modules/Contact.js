import axios from "axios";

export default {
  namespaced: true,
  state: { },
  actions: {
    CONTACT_US({commit}, payload) {
        var url = process.env.VUE_APP_APIBASEURL + "sendcontactus"
        var emailSubmission = {
            // payload
            email: payload.email,
            name: payload.name,
            subject: payload.subject,
            message: payload.message
        };
        axios
        .post(url, emailSubmission)
        .then(function () {
            console.log("Message sent!")
        })
        .catch(function (error) {
            console.log(error)
            console.log("Message not sent")
        })
        commit("CONTACT_US", payload)
      },
  },
  mutations: {
    CONTACT_US(state, payload){
        console.log(state)
        console.log(payload)
    }
  }
}