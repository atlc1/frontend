import axios from "axios";
export default {
  namespaced: true,
  state: {
    Fetched: [],
    RevisionList: [],
  },
  getters: {
    GET_REVISION_LIST_FETCHED: (state) => {
      return state.Fetched;
    },

    GET_REVISION_LIST: (state) => {
      return state.RevisionList;
    },
  },
  mutations: {
    ADD_REVISION_FETCHED(state, payload) {
      if (state.Fetched.indexOf(payload < 0)) {
        state.Fetched.push(payload);
      }
    },
    SET_REVISION_LIST(state, payload) {
      state.RevisionList = payload;
    },
    SET_FETCHED_LIST(state, payload) {
      state.Fetched = payload;
    },
  },
  actions: {
    RESET_REVISION_LIST({ commit }) {
      commit("SET_FETCHED_LIST", []);
      commit("SET_REVISION_LIST", {});
    },
    FETCH_REVISION_LIST({ commit, rootGetters }, payload) {
      var url;
      url = process.env.VUE_APP_APIBASEURL + "revision/ref-on/" + payload;

      axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          commit("ADD_REVISION_FETCHED", payload);      
          commit("SET_REVISION_LIST", response.data.Items);
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  },
};
