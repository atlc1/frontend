import axios from "axios";

export default {
  namespaced: true,
  state: {
    error: false,
    paymentResult: {},
    fetchOperations: {
      products: "waiting",
      subscriptionPlans: "waiting",
      subscriptionPlanSingle: "waiting",
    },
    paypalSubscriptionPlan: {},
    paypalProducts: {},
  },
  getters: {
    GET_PAYMENTERROR: (state) => {
      return state.error;
    },
    GET_PAYMENTRESULT: (state) => {
      return state.paymentResult;
    },
    GET_PAYPAL_SUBSCRIPTION_PLAN: (state) => {
      return state.paypalSubscriptionPlan;
    },
    GET_PAYPAL_PRODUCTS: (state) => {
      return state.paypalProducts;
    },
    GET_PAYPAL_FETCH_OPERATIONS: (state) => {
      return state.fetchOperations;
    },
  },
  mutations: {
    SET_PAYMENTERROR(state, payload) {
      state.error = payload;
    },
    SET_PAYMENTRESULT(state, payload) {
      state.paymentResult = payload;
    },
    SET_PAYPAL_SUBSCRIPTION_PLAN(state, payload) {
      console.log({ SET_PAYPAL_SUBSCRIPTION_PLAN: payload });
      state.paypalSubscriptionPlan = payload;
    },
    SET_PAYPAL_PRODUCTS(state, payload) {
      state.paypalProducts = payload;
    },
    SET_PAYPAL_FETCH_OPERATIONS(state, payload) {
      state.fetchOperations[payload["item"]] = payload["value"];
    },
  },
  actions: {
    PUSH_PAYMENTERROR({ commit }, payload) {
      commit("SET_PAYPAL_SUBSCRIPTION_PLAN", payload);
    },
    PUSH_PAYMENTRESULT({ commit }, payload) {
      commit("SET_PAYMENTRESULT", payload);
    },

    async FETCH_OR_CREATE_PAYPAL_SUBSRIPTION_PLAN(
      { commit, rootGetters },
      payload
    ) {
      var url;
      url =
        process.env.VUE_APP_PAYMENTAPIBASEURL +
        "payments/paypal/findorcreatebillingplan";
      commit("SET_PAYPAL_FETCH_OPERATIONS", {
        item: "subscriptionPlanSingle",
        value: "fetching",
      });

      await axios
        .post(url, payload, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          console.log(response);

          commit("SET_PAYPAL_SUBSCRIPTION_PLAN", response.data.plan);
          commit("SET_PAYPAL_FETCH_OPERATIONS", {
            item: "subscriptionPlanSingle",
            value: "success",
          });
        })
        .catch(function (error) {
          commit("SET_PAYPAL_FETCH_OPERATIONS", {
            item: "subscriptionPlanSingle",
            value: "error",
          });
          console.log(error);
        });
    },
    async FETCH_PAYPAL_SUBSRIPTION_PLAN({ commit, rootGetters }) {
      var url;
      url =
        process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/paypal/subscriptionplans";
      commit("SET_PAYPAL_FETCH_OPERATIONS", {
        item: "subscriptionPlans",
        value: "fetching",
      });

      await axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          console.log(response);

          commit("SET_PAYPAL_SUBSCRIPTION_PLAN", response.data.plans);
          commit("SET_PAYPAL_FETCH_OPERATIONS", {
            item: "subscriptionPlans",
            value: "success",
          });
        })
        .catch(function (error) {
          commit("SET_PAYPAL_FETCH_OPERATIONS", {
            item: "subscriptionPlans",
            value: "error",
          });
          console.log(error);
        });
    },
    async FETCH_PAYPAL_PRODUCTS({ commit, rootGetters }) {
      var url;
      url = process.env.VUE_APP_PAYMENTAPIBASEURL + "payments/paypal/products";
      commit("SET_PAYPAL_FETCH_OPERATIONS", {
        item: "products",
        value: "fetching",
      });

      await axios
        .get(url, rootGetters["UserStore/authHeader"])
        .then(function (response) {
          console.log(response);

          commit("SET_PAYPAL_PRODUCTS", response.data.products);
          commit("SET_PAYPAL_FETCH_OPERATIONS", {
            item: "products",
            value: "success",
          });
        })
        .catch(function (error) {
          commit("SET_PAYPAL_FETCH_OPERATIONS", {
            item: "products",
            value: "error",
          });
          console.log(error);
        });
    },
  },
};
