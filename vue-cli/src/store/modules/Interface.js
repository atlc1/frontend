export default {
  namespaced: true,
  state: {
    showPostCreate: false,
    menuDrawerOpen: false,
    showCommentCreateArray: ["placeholder"]
  },
  getters: {
    GET_SHOW_POSTCREATE: (state) => {
      return state.showPostCreate;
    },
    GET_MENU_DRAWER_OPEN: (state) => {
      return state.menuDrawerOpen;
    },
    GET_SHOW_COMMENTCREATE_ARRAY: (state) => {
      return state.showCommentCreateArray;
    },
    GET_IS_IN_ARRAY: (state, payload) => {
      return state.showCommentCreateArray.includes(payload)
    }
  },
  mutations: {
    SET_SHOW_POSTCREATE(state, payload) {
      state.showPostCreate = payload;
    },
    SET_MENU_DRAWER_OPEN(state, payload) {
      state.menuDrawerOpen = payload;
    },
    SET_SHOW_COMMENTCREATE_ARRAY(state, payload) {
      if(state.showCommentCreateArray.includes(payload)){
        let newCommentCreateArray = state.showCommentCreateArray.filter(item => item !== payload)
        state.showCommentCreateArray = newCommentCreateArray
      }
      else{
        state.showCommentCreateArray.push(payload)
      }
    },
    CLEAR_COMMENTCREATE_ARRAY(state){
      state.showCommentCreateArray = ["placeholder"]
    }
  },
  actions: {
    SET_SHOW_POSTCREATE({ commit }, payload) {
      commit("SET_SHOW_POSTCREATE", payload);
    },
    SET_SHOW_COMMENTCREATE_ARRAY({ commit }, payload) {
      commit("SET_SHOW_COMMENTCREATE_ARRAY", payload);
    },
    SET_MENU_DRAWER_OPEN({ commit }, payload) {
      commit("SET_MENU_DRAWER_OPEN", payload);
    },
    CLEAR_COMMENTCREATE_ARRAY({commit}){
      commit("CLEAR_COMMENTCREATE_ARRAY")
    }
  },
};
