import axios from "axios";

export default {
  namespaced: true,
  state: { 
    availableusergroups: ['user', 'meta-admin', 'tech-admin'],
    usergroups: [],
    debug: [],
    kicksuccessful: false
  },
  actions: {
    async KICK_USER({commit, dispatch, rootGetters}, payload){
      commit("CLEAR_DEBUG")
      console.log(payload)
      console.log("This is the payload to kick user")
      var url = "";
      if (payload == rootGetters["UserStore/userSlug"]) {
        url = process.env.VUE_APP_MODERATIONBASEURL + "usermanagement/kickuserself";
      } else {
        url = process.env.VUE_APP_MODERATIONBASEURL + "usermanagement/kickuser";
      }
      var data = {
        "userslug": payload
      };
      await axios
      .put(url, data, rootGetters["UserStore/authHeader"])
      .then(function () {
        commit("KICK_SUCCESSFUL")
        dispatch("ModerationStore/ARCHIVE_USER", payload, { root: true })
      })
      .catch(function (error) {
        console.log(error)  
        commit('ADD_DEBUG', "Could not kick user")
      })
    },
    UPDATE_USER_GROUPS({state, commit, rootGetters}, payload) {
      var url = process.env.VUE_APP_MODERATIONBASEURL + "updateusergroups"
        var data = {
            userslug: payload.userslug,
            usergroups: state.usergroups
        };
        axios
        .put(url, data, rootGetters["UserStore/authHeader"])
        .then(function () {
        })
        .catch(function (error) {
          commit('ADD_DEBUG', error)
            console.log(error)
            console.log("usergroups are not updated")
        })
        
      },
      GET_CURRENT_USER_GROUPS({ commit, rootGetters}, payload) {
        var url = process.env.VUE_APP_MODERATIONBASEURL + "getusergroups"
        var data_to_pass = { userslug: payload}
        axios
        .post(url, data_to_pass, rootGetters["UserStore/authHeader"])
        .then(function (resp) {
            commit('GET_CURRENT_USER_GROUPS', resp.data)
        })
        .catch(function (error) {
            console.log(error)
            console.log("unable to fetch user groups")
        })
      }, 
      UPDATE_STORE_VALUE({commit}, payload){
        commit("UPDATE_STORE_VALUE", payload)
      }
  },
  mutations: {
    GET_CURRENT_USER_GROUPS(state, payload){
      state.usergroups = payload
    },
    CLEAR_DEBUG(state){
      state.debug = []
    },
    UPDATE_STORE_VALUE(state, payload){
      state.usergroups = payload
    },
    ADD_DEBUG(state, payload){
      state.debug.push(payload)
    },
    KICK_SUCCESSFUL(state){
      state.kicksuccessful = true
    }
  },
  getters: {
    GET_USER_GROUPS(state){
      return state.usergroups
    },
    GET_AVAILABLE_USER_GROUPS(state){
      return state.availableusergroups
    },
    GET_DEBUG(state){
      return state.debug
    },
    GET_KICK_STATE(state){
      return state.kicksuccessful
    }
  }
}