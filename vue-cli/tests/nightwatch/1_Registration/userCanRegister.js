// This file is checking if a user can register
// This is based on User Story 1

// As a user
// I would like to create an account
// So that I can use WT.Social

// Nightwatch DOES NOT LIKE LOCALHOST
// I never figured out why. Please let me know if you do!

// var cp = require('child_process');
// var ls = cp.spawn('aws dynamodb delete-item --table-name WTS2AlphaAuth --key file://delete.json');

var cp = require('child_process');

cp.exec('./delete.sh'); //works

describe('Testing Registration', function() {

    afterEach(browser => browser.end());

    test('Can a user actually register?', function(browser) {
        browser
            .url('https://d2cgkfgsfyw731.cloudfront.net/')
            .click(".V-Tab-Register")
            .assert.visible("input[id='input-48']")
            // This input field is fname
            .setValue("input[id='input-48']", 'Apps')
            // This input field is lname
            .setValue("input[id='input-51']", 'Dev')
            // This input field is email
            .setValue("input[id='input-54']", 'apps@test.com')
            // This input field is password1
            .setValue("input[id='input-57']", 'inSecurePassword')
            // This input field is password2
            .setValue("input[id='input-61']", 'inSecurePassword')
            // Clicking 'Register'
            .click('button[class="register v-btn v-btn--is-elevated v-btn--has-bg theme--light v-size--default teal lighten-2"]')
            .assert.containsText('h3', 'Welcome Apps');
    });

});